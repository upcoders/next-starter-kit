import { colors, border, font, sizing } from '/css.js'

export default `

  .button {
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${ sizing.height.controlls };
    padding-left: 22px;
    padding-right: 22px;
    cursor: pointer;
    font-size: ${ font.size.button };
    font-family: ${ font.family.text };
    text-align: center;
    color: ${ colors.bg };
    background-color: ${ colors.active };
    border: ${ border.style } ${ border.width } ${ colors.active };
    border-radius: ${ border.radius };
    transition: color 1s ease, background-color .5s ease;
  }

  .button:hover {
    color: ${ colors.active };
    background-color: ${ colors.bg };
    transition: color 1s ease, background-color .5s ease;
  }

  .button.disabled {
    color: ${ colors.disabled };
    background-color: ${ colors.bg };
    border-color: ${ colors.disabled };
  }

`




