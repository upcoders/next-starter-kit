import 'react'
import CLASS from 'classnames'

import style from './Button.css.js'

const Button = ({ children, className, title, onClick }) => [
  <div
  	key='markup'
  	className={ CLASS(className, 'button') }
  	title={title ? title : "false"}
  	onClick={onClick}
  >{
  		children
  }</div>,
  <style key='style' jsx="true">{ style }</style>
]

export default Button
