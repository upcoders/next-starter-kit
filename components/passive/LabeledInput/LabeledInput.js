import React from 'react'
import CLASS from 'classnames'
import style from './LabeledInput.css.js'

import Input from '/components/passive/Input'

const LabeledInput = (props) => {
	const { label, labelClassName, children, className, ...inputProps } = props
  return [
    <label key='markup' className={ CLASS(labelClassName, 'labeledinput-label')}>
    	{ label }
    	<Input className={ CLASS(className, 'labeledinput-input' ) } { ...inputProps } />
    </label>,
    <style key='style' jsx="true">{ style }</style>
  ]

}

export default LabeledInput
