import { colors, border, font } from '/css.js'

export default `

	.labeledinput-label {
		color: #6c4527;
	}
	.labeledinput-input {
		margin-left: 10px;
	}

`
