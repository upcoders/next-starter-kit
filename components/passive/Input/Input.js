import { Component } from 'react'
import CLASS from 'classnames'

import style from './Input.css.js'

class Input extends Component {
	constructor(props){
		super(props)
		this.state = { value: props.value || '' }
		this.handle = this.handle.bind(this)
	}

	handle(event) {
		this.setValue(event.target.value)
	}

	setValue(value) {
		this.setState({ value })
		if ( typeof this.props.onChange === 'function' ) {
			this.props.onChange(value)
		}
	}

	componentWillReceiveProps(nextProps) {
		if ( nextProps.value !== this.props.value ) {
			this.setValue( nextProps.value )
		}
	}

	render() {
		const {
			className, type, value, title, placeholder,
			autoFocus, required, onFocus, onBlur } = this.props
		return [
			<input
				key='markup'
  			className={ CLASS(className, 'input')}
  			type={type || 'text'}
  			onChange={this.handle}
  			value={this.state.value}
  			{...{onFocus, onBlur, title, placeholder, autoFocus, required}}
  		/>,
	    <style key='style' jsx="true">{ style }</style>
	  ]
	}
}

export default Input
