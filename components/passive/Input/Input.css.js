import { colors, border, font, sizing } from '/css.js'

export default `

	.input {
		height: ${ sizing.height.controlls };
    padding-left: ${ sizing.paddings.input.left };
    font-size: ${ font.size.button };
    font-family: ${ font.family.text };
    color: ${ colors.input };
    background-color: ${ colors.bg };
    border: ${ border.style } ${ border.width } ${ colors.active };
    border-radius: ${ border.radius };
	}

`
