import React from 'react'
import CLASS from 'classnames'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { // from 'recompose'

/* -  Remove unused  - */

// Higher-order components
mapProps,
withProps,
withPropsOnChange,
withHandlers,
defaultProps,
renameProp,
renameProps,
flattenProp,
withState,
withStateHandlers,
withReducer,
branch,
renderComponent,
renderNothing,
shouldUpdate,
pure,
onlyUpdateForKeys,
onlyUpdateForPropTypes,
withContext,
getContext,
lifecycle,
toClass,

// Static property helpers
setStatic,
setPropTypes,
setDisplayName,

// Utilities
compose,
getDisplayName,
wrapDisplayName,
shallowEqual,
isClassComponent,
createSink,
componentFromProp,
nest,
hoistStatics,

// Observable utilities
componentFromStream,
componentFromStreamWithConfig,
mapPropsStream,
mapPropsStreamWithConfig,
createEventHandler,
createEventHandlerWithConfig,
setObservableConfig,

} from 'recompose'

import { createSelector } from 'reselect'

import style from './CityInput.css.js'

import actions from 'actions'

const {

  /* - remove unused - */

  
  addCount,
  setClock,
  serverRenderClock,
  startClock,
  booksLoading,
  booksLoaded,
  loadBooks,
  sushiLoading,
  sushiLoaded,
  loadSushi

} = actions

const CityInput = ({ children }) => {

  return [
    <div key='markup' className={ CLASS(className, 'cityinput')}>{ children }</div>,
    <style key='style' jsx="true">{ style }</style>
  ]

}

/* Uncomment only for pages or remove */

// CityInput.getInitialProps = async ({ store, isServer }) => {
//
//   return { isServer }
// }

export default compose(
  setDisplayName('CityInput'),
  connect( createSelector(
    (state, props) => state.branch[props.selectorProp]
  ), {

    /* actions */

  }),
  setPropTypes({
    children: PropTypes.array,
  }),
  pure
)(CityInput)
