import { colors, border, font } from '/css.js'

export default `

	.page-content {
	  width: 951.5px;
	  height: 487px;
	  padding: 10px;
	  background-color: ${ colors.bg };
	  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.5);
	  box-shadow: 0 1px 2px rgba(0,0,0,0.5);
	}

	.button-search {
		width: auto;
	}

	.city-input {
		width: 375px;
	}

`
