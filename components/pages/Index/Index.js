import { Component } from 'react'
import style from './Index.css.js'

import Button from '/components/passive/Button'
import LabeledInput from '/components/passive/LabeledInput'
// import Header from 'components/Header'
// import Logo from 'components/Logo'
// import Menu from 'components/Menu'

class Index extends Component {
	constructor(props){
		super(props)
		this.state = { v: 'start!' }
		this.ch = this.ch.bind(this)
	}

	ch(v) {
		console.log(v)
		this.setState({ v })
	}

	render() {
		return [
			<div key='markup' className="page-content" style={{display: 'flex'}}>
	    	<LabeledInput label={`${this.props.label}`} className="attached-left city-input" placeholder="test" onChange={this.ch}/>
		    <Button className="attached-right button-search" onClick={() => this.setState({v: 'clicked!'})}>Сменить город</Button>
		  </div>,
		  <style key='style' jsx="true">{ style }</style>
		]
	}
}

export default Index
