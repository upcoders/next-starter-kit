import style from './Layout.css.js'
export default ({ children }) => [ <div className="page">{ children }</div>, <style jsx="true">{ style }</style> ]
