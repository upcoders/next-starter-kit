import { colors, border, font } from '/css.js'

export default `

  ::-webkit-input-placeholder {color:#9d969e;}
  ::-moz-placeholder          {color:#9d969e;}
  :-moz-placeholder           {color:#9d969e;}
  :-ms-input-placeholder      {color:#9d969e;}

  input[placeholder]          {text-overflow:ellipsis;}
  input::-moz-placeholder     {text-overflow:ellipsis;}
  input:-moz-placeholder      {text-overflow:ellipsis;}
  input:-ms-input-placeholder {text-overflow:ellipsis;}

  input::-webkit-input-placeholder       {opacity: 1; transition: opacity 0.3s ease;}
  input::-moz-placeholder                {opacity: 1; transition: opacity 0.3s ease;}
  input:-moz-placeholder                 {opacity: 1; transition: opacity 0.3s ease;}
  input:-ms-input-placeholder            {opacity: 1; transition: opacity 0.3s ease;}
  input:focus::-webkit-input-placeholder {opacity: 0; transition: opacity 0.3s ease;}
  input:focus::-moz-placeholder          {opacity: 0; transition: opacity 0.3s ease;}
  input:focus:-moz-placeholder           {opacity: 0; transition: opacity 0.3s ease;}
  input:focus:-ms-input-placeholder      {opacity: 0; transition: opacity 0.3s ease;}

	.page {
		display: flex;
		justify-content: center;
		align-items: center;
  	min-height: 555px;
	}

	.attached-right {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }

  .attached-left {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  .attached-top {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }

  .attached-bottom {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }



`
