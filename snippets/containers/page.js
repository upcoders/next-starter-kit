import actions from 'actions'
import { connect } from 'react-redux'
import { selectLight, selectLastUpdate, selectCount } from 'selectors'
import { createSelector } from 'reselect'
import { compose, setDisplayName, pure } from 'recompose'
import Page from 'components/clockPage'

const { addCount } = actions;

export default compose(
  setDisplayName('ClockContainer'),
  connect(createSelector(
    selectLight(),
    selectLastUpdate(),
    selectCount(),
    (light, lastUpdate, count) => ({ light, lastUpdate, count })
  ), { addCount }),
  pure
)(Page)
