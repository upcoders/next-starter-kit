import React from 'react'
import CLASS from 'classnames'
import style from '_Component_.css.js'

const _Component_ = ({ children, className }) => {

  return [
    <div key='markup' className={ CLASS(className, '_component-class_')}>{ children }</div>,
    <style key='style' jsx="true">{ style }</style>
  ]

}

export default _Component_
