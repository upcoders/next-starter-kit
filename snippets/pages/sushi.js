import actions from 'actions'
import SushiPage from 'components/sushiPage'
import withRedux from 'next-redux-wrapper'
import { compose, setDisplayName, pure, lifecycle, withProps } from 'recompose'
import initStore from '../store'

const { loadSushi } = actions

const Sushi = compose(
  setDisplayName('Sushi'),
  withProps({
    title: 'Sushi page'
  }),
  pure
)(SushiPage)

Sushi.getInitialProps = async ({ store, isServer }) => {
  await loadSushi(store.dispatch, store.getState)
  return { isServer }
}

export default withRedux(initStore, null, null)(Sushi)
