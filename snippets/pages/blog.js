import actions from 'actions'
import BooksPage from 'components/booksPage'
import withRedux from 'next-redux-wrapper'
import { compose, setDisplayName, pure, lifecycle, withProps } from 'recompose'
import initStore from '../store'

const { loadBooks } = actions

const Blog = compose(
  setDisplayName('Blog'),
  withProps({
    title: 'Blog page'
  }),
  pure
)(BooksPage)

Blog.getInitialProps = async ({ store, isServer }) => {
  await loadBooks(store.dispatch, store.getState)
  return { isServer }
}

export default withRedux(initStore, null, null)(Blog)
