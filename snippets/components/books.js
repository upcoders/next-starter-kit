import React from 'react'
import PropTypes from 'prop-types'
import { compose, pure, setDisplayName, setPropTypes } from 'recompose'
import Book from './book'
import { connect } from 'react-redux'
// import sass from 'modules/sass'
// import css from './books.sass'

const style =

console.log(style)

const Books = ({ books }) => {
  let items = [];
  if(books) items = books.map(book => <Book key={book.ID} {...book}/>)
  return (
    <div className='books'>

      {items}
      <style jsx="true">{`
        .books {
          display: flex;
          flex-wrap: wrap;
          width: 100%;
        }
      `}</style>

    </div>
  )
}

export default compose(
  setDisplayName('Book'),
  connect( state => ({ ...state.books })),
  setPropTypes({
    books: PropTypes.array,
  }),
  pure
)(Books)
