import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { compose, setDisplayName, pure, setPropTypes } from 'recompose'
import Books from './books'

const BooksPage = () => {
  return (
    <div>
      <Books />
    </div>
  );
}

export default compose(
  setDisplayName('BooksPage'),
  pure
)(BooksPage)
