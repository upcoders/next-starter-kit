import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { compose, setDisplayName, pure, setPropTypes } from 'recompose'
import { connect } from 'react-redux'

const SushiPage = ({ sushi }) => {
  return (
    <div className="cards">
      {
        sushi.map(({
          id, title, weight, is_new, popular, position, is_delivery, toppings, price, price_old, is_action, composition, images
         }) => {
          return images.length ? (
            <a href="/spb/menu/rolls/kaliforniya-v-kunzhute/" className="card-wrapper--grid" key={id} data-id="435">
              <div className="card--grid">
                <div className="card__newbox">
                  <p className="card__newbox--new"></p></div>
                <div className="favorite-control">
                  <div>
                    <div className="favorite-control__controls">
                      <span className="fav"></span></div></div></div>
                <div>
                  <div className="card__image" title={ title } style={{
                    backgroundImage: `url("https://new.sushiwok.ru/img/${images[0].filename}/485x485")`
                  }}></div>
                  <div className="card__main-content">
                    <div className="card__title-content">
                      <p className="card__name js-content">{ title }</p>
                      <div className="card__card-icons"></div></div>
                    <div className="card__more-content">
                      { is_action ? <span className="card__is-action">акция, </span> : '' }
                      <span className="card__ingredients js-content" data-id="435">{
                        composition.reduce((comp, ing) => `${comp ? comp + ',' : ''} ${ing.title}`, "")
                      }</span>
                      <span className="card__weight">{ weight }&nbsp;г</span></div></div></div>
                <div className="card__price__and__buybutton">
                  <div className="card__price--stock">
                    <span className="card__price__current--is-action">{ price } р.</span>
                    <span className="card__price__old">{ price_old } р.</span></div>
                  <div className="card__buy-action">
                    <div className="card__button">в корзину<span className="card__button-add"></span></div></div></div></div></a>
          ) : null
        })
      }
      <style jsx>{`

      .cards {
        display: flex;
        flex-wrap: wrap;
      }

      .card-wrapper--grid {
        position: relative;
        box-sizing: border-box;
        vertical-align: top;
        color: #000;
        text-decoration: none;
        font-size: 1rem;
        margin: 4px;
        width: calc(100% / 4 - 9px);
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
      }
      .card--grid {
        cursor: pointer;
        box-sizing: border-box;
        position: relative;
        border: 1px solid #efefef;
        border-radius: 3px;
        width: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        transition: box-shadow .3s ease;

      }
      .card__newbox {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
      }
      .card__newbox--new {
        position: absolute;
        top: 0;
        right: 0;
        color: #2ca215;
        text-transform: uppercase;
        font-weight: 700;
        margin: 0;
        padding-top: 24px;
        padding-right: 20px;
      }
      .favorite-control {
        position: absolute;
      }
      .favorite-control__controls {

      }
      .fav {

      }
      .card__image {
        height: 250px;
        font-size: 0;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        transition-property: top,right;
        transition-duration: 5s;

      }
      .card__main-content {
        padding: 20px;
        padding-top: 0;
        position: relative;
      }
      .card__title-content {
        font-size: 21px;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
      }
      .card__name.js-content {
        font-size: 20px;
        margin: 0;
      }
      .card__card-icons {
        text-align: right;
      }
      .card__more-content {
        color: #8d8c8d;
        font-size: 14px;
        padding-top: 4px;
      }
      .card__is-action {
        font-size: 14px;
        color: #da0002;
      }
      .card__ingredients js-content {

      }
      .card__weight {
        color: #8d8c8d;
        font-size: 14px;
        padding-top: 4px;
      }
      .card__price__and__buybutton {
        padding: 20px;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-top: auto;
        padding: 20px;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
      }
      .card__price--stock {
        position: relative;
        top: 2px;
      }
      .card__price__current--is-action {
        display: block;
        font-size: 31px;
        padding: 4px 18px 0 20px;
        margin-left: -20px;
        color: #901e1f;
        font-weight: 500;
        margin-bottom: -4px;
      }
      .card__price__old {
        text-decoration: line-through;
        padding: 0 .5rem 0 0;
        color: #a9a9a9;
        font-weight: 100;
        margin-top: 5px;
        display: inline-block;
        font-size: 16px;
      }
      .card__buy-action {
        text-align: center;
        margin-top: 4px;
        max-width: 147px;
        min-width: 147px;
      }
      .card__button {
        cursor: pointer;
        position: relative;
        width: 100%;
        height: 48px;
        padding-right: 42px;
        padding-left: 18px;
        font-size: 15px;
        text-align: left;
        color: #da0002;
        border: 2px solid;
        border-radius: 50px;
        text-transform: uppercase;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        display: inline-block;
        font-weight: 500;
        box-sizing: border-box;
        line-height: 44px;
      }
      .card__button:before {
        position: absolute;
        display: block;
        content: "";
        background: #da0002;
        right: 13px;
        top: 21px;
        width: 18px;
        height: 2px;
      }
      .card__button:before {
        right: 21px;
        top: 13px;
        width: 2px;
        height: 18px;
        position: absolute;
        display: block;
        content: "";
        background: #da0002;
      }
      .card__button-add {
        cursor: pointer;
        position: relative;
        width: 100%;
        height: 48px;
        padding-right: 42px;
        padding-left: 18px;
        font-size: 15px;
        text-align: left;
        color: #da0002;
        border: 2px solid;
        border-radius: 50px;
        text-transform: uppercase;
        user-select: none;
        display: inline-block;
        font-weight: 500;
        box-sizing: border-box;
        line-height: 44px;
      }

    `}</style>
    </div>
  );
}

export default compose(
  setDisplayName('SushiPage'),
  connect( state => ({ ...state.sushi })),
  setPropTypes({
    sushi: PropTypes.array,
  }),
  pure
)(SushiPage)
