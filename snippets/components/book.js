import React from 'react'
import PropTypes from 'prop-types'
import { compose, pure, setDisplayName, setPropTypes } from 'recompose'
// import CSS from './book.sass'

// const css = Object.keys(CSS).reduce((names, name) => ({
//   ...names,
//   [ name.replace(/-([a-z])/g, (s, char) => char.toUpperCase()) ] : CSS[ name ]
// }));

const Book = ({Title, Description, PageCount, PublishDate, Url }) =>
  <div className='book'>
    <img className='book-cover' src={Url} alt="Title"/>
    <div className='book-info'>
      <div className='book-title'>{Title}</div>
      <div className='book-description'>{Description}</div>
      <div className='book-pages'>Pages count: {PageCount}</div>
      <div className='book-published'>Published at: {PublishDate.replace(/^(\d{4}\-\d{2}\-\d{2}).*/, '$1')}</div>
    </div>

    <style jsx>{`
      .book {
        display: flex;
        width: calc(100% / 4 - 50px);
        background-color: #987;
        box-shadow: 0 4px 4px rgba(0,0,0,0.7);
        padding: 20px;
        margin: 5px;
      }

      .book-cover {
        height: 180px;
        margin: 5px;
      }

      .book-info {
        padding: 5px;
      }

      .book-title {
        font-size: 32px;
        color: #456;
        text-align: center;
      }

      .book-pages {
        margin-top: 20px;
      }

      .book-description {
        height: 80px;
        overflow: hidden;
      }
      .book-pages,
      .book-published,
      .book-description {
        font-size: 18px;
        color: #444;
      }

    `}</style>

  </div>

export default compose(
  setDisplayName('Book'),
  setPropTypes({
    Title: PropTypes.string,
    Description: PropTypes.string,
    PageCount: PropTypes.number,
    PublishDate: PropTypes.string,
    Url: PropTypes.string
  }),
  pure
)(Book)
