import 'isomorphic-unfetch'
import { actionTypes } from 'reducers/books'

const { BOOKS_LOADING, BOOKS_LOADED } = actionTypes;

const actions = {

	booksLoading: () => ({ type: BOOKS_LOADING }),

	booksLoaded: data => ({ type: BOOKS_LOADED, data }),

	async loadBooks(dispatch, getState) {
		const { books } = getState().books
		if(books === null){
			dispatch(actions.booksLoading())
			const res = await fetch('http:\/\/fakerestapi.azurewebsites.net/api/Books')
			const booksData = await res.json()
			const booksFetchCovers = booksData.reduce((fetches, book) => [...fetches, fetch(`http:\/\/fakerestapi.azurewebsites.net/api/CoverPhotos/${book.ID}`)], [])
			const booksFetchedCovers = await Promise.all(booksFetchCovers);
			const booksParseCovers = booksFetchedCovers.reduce((covers, result) => [...covers, result.json()], []);
			const booksParsedCovers = await Promise.all(booksParseCovers);
			const books = booksData.reduce((booksWithCovers, book) => {
				return [
					...booksWithCovers, {
						...booksParsedCovers.filter(cover => cover.IDBook === book.ID).shift(),
						...book
					}
				]
			}, {})
			dispatch(actions.booksLoaded(books))
		}
	}

}

export default actions;
