import { actionTypes } from 'reducers/count'

const { ADD, TICK } = actionTypes;

const actions = {

	addCount: () => ({ type: ADD }),

	setClock: (light, ts) => ({ type: TICK, data: { light, ts } }),

	serverRenderClock: isServer => dispatch => dispatch(actions.setClock(!isServer, Date.now())),

	startClock: () => dispatch => setInterval(() => dispatch(actions.setClock(true, Date.now())), 800)

}

export default actions;
