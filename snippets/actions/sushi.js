import 'isomorphic-unfetch'
import { actionTypes } from 'reducers/sushi'

const { SUSHI_LOADING, SUSHI_LOADED } = actionTypes;

const actions = {

	sushiLoading: () => ({ type: SUSHI_LOADING }),

	sushiLoaded: data => ({ type: SUSHI_LOADED, data }),

	async loadSushi(dispatch, getState) {
		const { sushi } = getState().sushi
		if(sushi === null){
		    dispatch(actions.sushiLoading())
		    const result = await fetch('https:\/\/new.sushiwok.ru/api/ru/spb/product/get_list', {
		    	headers: { "User-Agent": "test1231" }
		    })
		    const { data } = await result.json()
		    dispatch(actions.sushiLoaded(data.product))
		}
	}

}

export default actions;
