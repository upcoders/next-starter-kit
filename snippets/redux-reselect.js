import React from 'react'
import CLASS from 'classnames'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'

import style from './Component.css.js'

import actions from 'actions'

const {

  /* - remove unused - */

  _Actions_
} = actions

const _Component_ = ({ children }) => {

  return [
    <div key='markup' className={ CLASS(className, '_component-class_')}>{ children }</div>,
    <style key='style' jsx="true">{ style }</style>
  ]

}

/* Uncomment only for pages or remove */

// _Component_.getInitialProps = async ({ store, isServer }) => {
//
//   return { isServer }
// }

export default connect( createSelector(
  (state, props) => state.branch[props.selectorProp]
), {

  /* actions */

})(_Component_)
