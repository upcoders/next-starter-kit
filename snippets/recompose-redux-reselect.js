import React from 'react'
import CLASS from 'classnames'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { // from 'recompose'

/* -  Remove unused  - */

// Higher-order components
mapProps,
withProps,
withPropsOnChange,
withHandlers,
defaultProps,
renameProp,
renameProps,
flattenProp,
withState,
withStateHandlers,
withReducer,
branch,
renderComponent,
renderNothing,
shouldUpdate,
pure,
onlyUpdateForKeys,
onlyUpdateForPropTypes,
withContext,
getContext,
lifecycle,
toClass,

// Static property helpers
setStatic,
setPropTypes,
setDisplayName,

// Utilities
compose,
getDisplayName,
wrapDisplayName,
shallowEqual,
isClassComponent,
createSink,
componentFromProp,
nest,
hoistStatics,

// Observable utilities
componentFromStream,
componentFromStreamWithConfig,
mapPropsStream,
mapPropsStreamWithConfig,
createEventHandler,
createEventHandlerWithConfig,
setObservableConfig,

} from 'recompose'

import { createSelector } from 'reselect'

import style from './_Component_.css.js'

import actions from 'actions'

const {

  /* - remove unused - */

  _Actions_
} = actions

const _Component_ = ({ children }) => {

  return [
    <div key='markup' className={ CLASS(className, '_component-class_')}>{ children }</div>,
    <style key='style' jsx="true">{ style }</style>
  ]

}

/* Uncomment only for pages or remove */

// _Component_.getInitialProps = async ({ store, isServer }) => {
//
//   return { isServer }
// }

export default compose(
  setDisplayName('_Component_'),
  connect( createSelector(
    (state, props) => state.branch[props.selectorProp]
  ), {

    /* actions */

  }),
  setPropTypes({
    children: PropTypes.array,
  }),
  pure
)(_Component_)
