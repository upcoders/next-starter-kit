const reducers = {

  TICK: (state, { ts, light }) => ({...state, lastUpdate: ts, light: !!light}),

  ADD: state => ({...state, count: state.count + 1})

}

export const initialState = {
  lastUpdate: 0,
  light: false,
  count: 0
}

export const actionTypes = Object.keys(reducers).reduce((types, type) => ({...types, [type]: type}), {});
export default (state = initialState, { type, data }) => (reducers[type] || (state => state))(state, data);
