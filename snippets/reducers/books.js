export const initialState = {
  loading: false,
  books: null
}

const reducers = {

  BOOKS_LOADING: state => ({ ...state, loading: true }),

  BOOKS_LOADED: (state, books) => ({ loading: false, books })

}

export const actionTypes = Object.keys(reducers).reduce((types, type) => ({...types, [type]: type}), {});
export default (state = initialState, { type, data }) => (reducers[type] || (state => state))(state, data);
