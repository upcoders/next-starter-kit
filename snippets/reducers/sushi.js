export const initialState = {
  loading: false,
  sushi: null
}

const reducers = {

  SUSHI_LOADING: state => ({ ...state, loading: true }),

  SUSHI_LOADED: (state, sushi) => ({ loading: false, sushi })

}

export const actionTypes = Object.keys(reducers).reduce((types, type) => ({...types, [type]: type}), {});
export default (state = initialState, { type, data }) => (reducers[type] || (state => state))(state, data);
