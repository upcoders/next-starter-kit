import clockActions from './clock'
import booksActions from './books'
import sushiActions from './sushi'

export default {

	...clockActions,
	...booksActions,
	...sushiActions

}
