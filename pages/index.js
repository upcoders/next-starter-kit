import { compose, setDisplayName, pure } from 'recompose'
// import withRedux from 'next-redux-wrapper'
// import initStore from '/store'

import Index from '/components/pages/Index'

const IndexPage = compose( setDisplayName('IndexPage'), pure )(Index)

IndexPage.getInitialProps = async ({ store, isServer }) => {
  return { isServer, label: 'Ваш город:' }
}

export default IndexPage
