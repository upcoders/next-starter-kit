import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

import Layout from '/components/pages/Layout'

const ga = () => `
(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='\/\/www.google-analytics.com\/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','UA-XXXXX-X','auto');ga('send','pageview');`

export default class MyDocument extends Document {
  static getInitialProps (props) {
    return Document.getInitialProps (props)
  }

  render () {
    return (
      <html>
        <Head>
          <meta charSet="utf-8"/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
          <title>Булошная Жан Руа</title>
          <meta name="description" content="Булошная Жан Руа"/>
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
          <link rel="apple-touch-icon" href="apple-touch-icon.png"/>
          <link rel="stylesheet" href="/static/css/normalize.min.css"/>
          <link rel="stylesheet" href="/static/css/main.css"/>
        </Head>
        <body>
          <Layout>
            <Main />
          </Layout>

          <NextScript />
          <script dangerouslySetInnerHTML={{ __html: ga()}} />

        </body>
      </html>
    )
  }
}

const styles = flush()
