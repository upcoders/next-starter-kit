require('babel-register')
const fs = require('fs')
const path = require('path')
const readline = require('readline')
const actions = require('./actions')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const getFiles = config => ({
	from: {
		path: './snippets',
		files:{
			style: 'style.css.js',
			package: 'package.json',
			js: (({ recompose, redux, reselect = false }) => {
		    const api = recompose || redux || reselect ? { recompose, redux, reselect } : { pure: true }
		    console.log(api)
		    return Object.keys(api).filter(feature => api[feature]).join('-') + '.js'
		  })(config)
		}
	},
	to: {
		path: `./components/${config.type}/${config.name}`,
		files: {
			style: `${config.name}.css.js`,
			package: 'package.json',
			js: `${config.name}.js`
		}
	}
})


const q = (req, name, opts) => result => {
	result = result || {};
	return new Promise((ok, fail) => {
		rl.question(`${
			req
		} ${
			opts instanceof Array
			? `[${opts.join(', ')}]`
			: typeof opts === 'boolean'
				? '? (yes|no)'
				: ''
		}: `, ans => {
			let res;
			if (opts instanceof Array) {
				if (opts.indexOf(ans) === -1){
					console.log('\u001b[31mWrong answer! try nagain...\u001b[0m')
					return ok(q(req, name, opts)(result))
				} else {
					result[name] = ans
					return ok(result)
				}
			} else if (typeof opts === 'boolean') {
				result[name] = /^(yes|1|да|ok|ок)$/i.test(ans.toString())
				return ok(result)
			} else {
				if (ans) {
					result[name] = ans
					return ok(result)
				} else {
					console.log('\u001b[31mWrong answer! try nagain...\u001b[0m')
					return ok(q(req, name, opts)(result))
				}
			}
		})
	})
}

const normalize = result => ({ ...result, type: result.type === 'page' ? 'pages' : result.type })

q('Component name', 'name')()
.then(q('Component type', 'type', ['active', 'passive', 'HOC', 'page']))
.then(q('Add recompose support?', 'recompose', true))
.then(q('Connect to redux store?', 'redux', true))
.then(result => result.redux ? q('Use reselect?', 'reselect', true)(result) : Promise.resolve(result))
.then(q('Author', 'author'))
.then(result => {
	result = normalize(result)
	const filesMap = getFiles(result)
	console.log(result)
	console.log(filesMap)
	Object.keys(filesMap.from.files).forEach(file => {
		fs.mkdir(filesMap.to.path, err => {
			fs.readFile(path.resolve(filesMap.from.path, filesMap.from.files[file]), (err, content) => {
				fs.writeFile(
				  path.resolve(filesMap.to.path, filesMap.to.files[file]),
				  content
					.toString()
					.replace(/_Component_/g, result.name)
					.replace(/_component-class_/g, result.name.toLowerCase())
					.replace(/_Author_/g, result.author)
					.replace(/_Actions_/g, `\n  ${Object.keys(actions.default).join(',\n  ')}\n`),
					err => err ? console.log(err) : console.log(`${path.resolve(filesMap.from.path, filesMap.to.files[file])} created!`)
				)
			})
		})
	})
	rl.close()
})
