export const colors = {
	bg: '#fff',
	active: '#46c2c6',
	input: '#777078',
	selected: '#8c6e54',
	special: '#775438',
	disabled: '#bc9776',
	header: '#6c4527',
	attention: '#d9ae88',
	attentionBg: '#443029'
}

export const border = {
	width: '1px',
	radius: '5px',
	style: 'solid'
}

export const font = {
	family: {
		text: 'Wien',
		menu: 'NautilusPompiliusRegular'
	},
	size: {
		button: '21px'
	}
}

export const sizing = {
	paddings: {
		button: {
			top: '15px',
			bottom: '14px'
		},
		input: {
			left: '19px'
		}
	},
	height: {
		controlls: '50px'
	}
}
