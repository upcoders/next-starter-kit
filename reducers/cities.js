import { getActionTypes, connectReducers } from '/modules/redux/refine'

export const initialState = {
  loading: false,
  currentCity: {
  	name: 'Санкт-Перербург',
  	path: 'spb'
  },
  cities: null
}

const reducers = {

  CITIES_LOADING: state => ({ ...state, loading: true }),

  CITIES_LOADED: (state, cities) => ({ loading: false, cities }),

  SELECT_CITY: (state, city) => ({
  	...state,
  	currentCity: cities.find(city => city.path === city || city.name === city) || state.currentCity
  })

}

export const actionTypes = Object.keys(reducers).reduce((types, type) => ({...types, [type]: type}), {});
export default (state = initialState, { type, data }) => (reducers[type] || (state => state))(state, data);
