import { combineReducers } from 'redux'
import count, { initialState as countState } from './count'
import books, { initialState as booksState } from './books'
import sushi, { initialState as sushiState } from './sushi'

export const intitialState = {
  count: countState,
  books: booksState,
  sushi: sushiState
}

export default combineReducers({
  count, books, sushi
})
